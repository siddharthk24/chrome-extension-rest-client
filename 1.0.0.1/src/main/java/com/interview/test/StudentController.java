package com.interview.test;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/hello")
public class StudentController {
	
    @RequestMapping(value = "/getStudents", method = RequestMethod.GET)
    @ResponseBody
    public String getStudentDetails(ModelMap model) {
    	System.out.println("Get method is called");
        return "welcome to get method!";
    }
 
    @RequestMapping(value = "/updateStudent", method = RequestMethod.POST)
    @ResponseBody
    public String updateStudentDetails(ModelMap model) {
    	System.out.println("Get method is called");
        return "welcome to POST  method!";
    }
    
    @RequestMapping(value = "/deleteStudent", method = RequestMethod.DELETE)
    @ResponseBody
    public String sayHelloAgain(ModelMap model) {
    	System.out.println("Get method is called");
        return "welcome to DLETE method!";
    }
}