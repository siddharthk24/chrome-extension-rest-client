package com.interview.test;

import java.util.HashMap;
import java.util.Map;

 class Student {
	private String Name;
 	private String id;
 	
 	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Student(String Name,String id){
 		super();
		this.Name=Name;
 		this.id=id;
 	}

 }
