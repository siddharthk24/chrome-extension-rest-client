package com.interview.test;

import java.util.HashMap;
import java.util.Map;

public class StudentService {
	
	Map<String, Student> StudentDb;
		
	public StudentService(){
		StudentDb =new HashMap<String, Student>();
		StudentDb.put("1",new Student("sid","1"));
		StudentDb.put("2",new Student("saya","2"));
	}
	
	public Map<String, Student> ReturnStudent(){
		return StudentDb;
	}
}
